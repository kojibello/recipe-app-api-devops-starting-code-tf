
# CREATE ALB 
resource "aws_security_group" "lb" {

  description = "Allow access to Application Load Balancer"
  name        = "${var.component_name}-alb-SG"
  vpc_id      = local.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 8080
    to_port     = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.component_name}_alb-SG"
  }
}

resource "aws_security_group" "ecs" {
  vpc_id      = local.vpc_id
  name        = "${var.component_name}_ecs_sg"
  description = "Allow inbound HTTP traffic to Jenkins from ALB only"

  ingress {
    description     = "allow jenkins master node from port ${var.container_port} only"
    from_port       = var.container_port
    to_port         = var.container_port
    protocol        = "tcp"
    security_groups = [aws_security_group.lb.id]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = local.operational_state.private_subnets_cidrs
  }

  tags = {
    Name = "${var.component_name}_ecs_sg"
  }
}

resource "aws_security_group" "db_sg" {
  name        = "${var.component_name}-postgres-db-sg"
  description = "allow ecs sg cluster"
  vpc_id      = local.vpc_id
  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = "${var.component_name}-db-sg"
  }
}

resource "aws_security_group_rule" "postgres_port_allow_ecs" {
  security_group_id        = aws_security_group.db_sg.id
  description              = "Allow alb ingress access to db cluster on port"
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.ecs.id
}
