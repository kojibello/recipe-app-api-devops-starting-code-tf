
resource "aws_lb" "albdev" {
  name                       = "alb"
  load_balancer_type         = "application"
  security_groups            = [aws_security_group.lb.id]
  subnets                    = local.public_subnet
  enable_deletion_protection = false

  tags = {
    Name = "${var.component_name}_alb"
  }
}

resource "aws_lb_listener" "api" {
  load_balancer_arn = aws_lb.albdev.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_target_group" "albdev" {
  name = "tg"

  port        = var.container_port
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = local.vpc_id
  # load_balancing_algorithm_type = var.target_group_load_balancing_algorithm_type
  health_check {
    healthy_threshold   = "2"
    interval            = "90"
    protocol            = "HTTP"
    port                = "8080"
    matcher             = "200"
    timeout             = "60"
    path                = "/student/"
    unhealthy_threshold = "7"
  }
  tags = {
    Name = "${var.component_name}_tg"
  }
}

resource "aws_lb_listener" "api_https" {

  load_balancer_arn = aws_lb.albdev.arn
  port              = 443
  protocol          = "HTTPS"
  certificate_arn   = module.acm.acm_certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.albdev.arn
  }
}
