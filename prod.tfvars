aws_account_id = {
  prod = "735972722491"
}

dns_zone_name             = "kojitechs.com"
subject_alternative_names = ["*.kojitechs.com"]
container_version         = "latest"
master_username           = "recipeapp"
database_name             = "recipeapp"
