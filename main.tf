
terraform {
  required_version = ">=v1.3.1"

  backend "s3" {
    bucket         = "hqr.common.database.module.kojitechs.tf"
    dynamodb_table = "terraform-lock"
    key            = "path/env/recipe-app"
    region         = "us-east-1"
    encrypt        = "true"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  assume_role {
    role_arn = "arn:aws:iam::${lookup(var.aws_account_id, terraform.workspace)}:role/Role_For-S3_Creation"
  }
  default_tags {
    tags = module.required_tags.aws_default_tags
  }
}

data "terraform_remote_state" "operational_environment" {
  backend = "s3"

  config = {
    region = "us-east-1"
    bucket = "operational.vpc.tf.kojitechs"
    key    = format("env:/%s/path/env", terraform.workspace)
  }
}

locals {
  operational_state = data.terraform_remote_state.operational_environment.outputs
  vpc_id            = local.operational_state.vpc_id
  public_subnet     = local.operational_state.public_subnets
  private_subnets   = local.operational_state.private_subnets
  db_subnets_names  = local.operational_state.db_subnets_names
  name              = "kojitechs-${replace(basename(var.component_name), "_", "-")}"
  cluster_secrets   = data.aws_secretsmanager_secret_version.rds_secret_target
}

data "aws_secretsmanager_secret_version" "rds_secret_target" {
  depends_on = [module.aurora]
  secret_id  = module.aurora.secrets_version
}

data "aws_region" "current" {}

data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}."
}

module "required_tags" {
  source = "git::https://github.com/Bkoji1150/kojitechs-tf-aws-required-tags.git?ref=v1.0.0"

  line_of_business        = var.line_of_business
  ado                     = var.ado
  tier                    = var.tier
  operational_environment = upper(terraform.workspace)
  tech_poc_primary        = var.tech_poc_primary
  tech_poc_secondary      = var.builder
  application             = var.application
  builder                 = var.builder
  application_owner       = var.application_owner
  vpc                     = var.vpc
  cell_name               = var.cell_name
  component_name          = var.component_name
}

module "aurora" {
  source = "git::https://github.com/Bkoji1150/aws-rdscluster-kojitechs-tf.git?ref=v1.1.11"

  component_name      = var.component_name
  name                = local.name
  engine              = "aurora-postgresql"
  publicly_accessible = true
  engine_version      = "11.15"
  instances = {
    1 = {
      instance_class      = "db.r5.2xlarge"
      publicly_accessible = false
    }
  }

  vpc_id                 = local.vpc_id
  create_db_subnet_group = true
  subnets                = local.private_subnets
  vpc_security_group_ids = [aws_security_group.db_sg.id]

  iam_database_authentication_enabled = true
  apply_immediately                   = true
  skip_final_snapshot                 = true
  enabled_cloudwatch_logs_exports     = ["postgresql"]
  database_name                       = var.database_name
  master_username                     = var.master_username
}

module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "3.0.0"

  domain_name               = trimsuffix(data.aws_route53_zone.zone.name, ".")
  zone_id                   = data.aws_route53_zone.zone.zone_id
  subject_alternative_names = var.subject_alternative_names
}

resource "aws_route53_record" "app" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "${var.component_name}.${var.dns_zone_name}"
  type    = "A"

  alias {
    name                   = aws_lb.albdev.dns_name
    zone_id                = aws_lb.albdev.zone_id
    evaluate_target_health = true
  }
}

resource "aws_ecs_cluster" "api" {
  name = "${var.component_name}-cluster"
}

resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "ecs/aws/${var.component_name}"

  tags = {
    Name = "${var.component_name}"
  }
}

resource "aws_ecs_task_definition" "api" {
  family = "${var.component_name}_task_def"
  requires_compatibilities = [
    "FARGATE",
  ]
  execution_role_arn = aws_iam_role.iam_for_ecs.arn
  task_role_arn      = aws_iam_role.iam_for_ecs.arn
  network_mode       = "awsvpc"
  cpu                = 1024
  memory             = 2048

  volume {
    name = "static"
  }
  container_definitions = jsonencode([
    {
      name = "${var.component_name}-api"
      image = lower(var.container_image_source) == "ecr" ? format(
        "%s.dkr.ecr.us-east-1.amazonaws.com/%s:%s",
        var.ecr_account_id,
        var.container_api_name,
        var.container_version
      ) : ""

      essential = true
      mountPoints = [
        {
          containerPath = "/vol/static"
          sourceVolume  = "static"
        }
      ],
      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = "${aws_cloudwatch_log_group.ecs_task_logs.name}",
          awslogs-region        = "${data.aws_region.current.name}",
          awslogs-stream-prefix = "${aws_cloudwatch_log_group.ecs_task_logs.name}-api"
        }
      },
      environment = [
        {
          name  = "DJANGO_SECRET_KEY"
          value = var.django_secret_key
        },
        {
          name  = "DB_HOST"
          value = jsondecode(local.cluster_secrets.secret_string)["endpoint"]
        },
        {
          name  = "DB_NAME"
          value = jsondecode(local.cluster_secrets.secret_string)["dbname"]
        },
        {
          name  = "DB_USER"
          value = jsondecode(local.cluster_secrets.secret_string)["username"]
        },
        {
          name  = "DB_PASS"
          value = jsondecode(local.cluster_secrets.secret_string)["password"]
        },
        {
          name  = "ALLOWED_HOSTS"
          value = "${aws_route53_record.app.fqdn}"
        }
      ]
      portMappings = [
        {
          containerPort = var.container_port 
          hostPort      = var.container_port
        }
      ]
    }
    ],
  )
}

resource "aws_ecs_service" "api" {
  name             = upper("${var.component_name}-service")
  cluster          = aws_ecs_cluster.api.name
  task_definition  = aws_ecs_task_definition.api.family
  desired_count    = 1
  platform_version = "1.4.0"
  launch_type      = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.ecs.id]
    subnets          = local.private_subnets
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.albdev.arn
    container_name   = "${var.component_name}-api"
    container_port   = var.container_port
  }
}
