
variable "component_name" {
  description = "Name of the component."
  type        = string
  default     = "student-api"
}

variable "image_jenkins_ecs_master" {
  type        = string
  description = "(optional) describe your variable"
  default     = ""
}

###### 
variable "aws_account_id" {
  description = "Environment this template would be deployed to"
  type        = map(string)
  default     = {}
}

variable "dns_zone_name" {
  description = "Domain name"
}

variable "ecr_account_id" {
  type        = string
  description = "The ID of the account to which the ECR repository belongs."
  default     = "674293488770"
}

variable "subject_alternative_names" {
  type    = list(any)
  default = []
}

variable "container_image_source" {
  description = "Either `dockerhub` or `ecr`."
  type        = string
  default     = "ecr"
}

# variable "container_proxy_name" {
#   description = "The name of the container as is in `ecr`"
#   type        = string
#   default     = "recipe-app-api-proxy"
# }

variable "container_api_name" {
  description = "The name of the container as is in `ecr`"
  type        = string
  default     = "django-api-framework"
}


variable "container_version" {
  type        = string
  description = "The image used to start a container. Up to 255 letters (uppercase and lowercase), numbers, hyphens, underscores, colons, periods, forward slashes, and number signs are allowed."
}

variable "container_image" {
  type        = string
  description = "The image used to start the container. Images in the Docker Hub registry available by default"
  default     = null
}

# Service Configuration
variable "container_port" {
  description = "Port that this service will listen on."
  type        = number
  default     = 8080
}

variable "worker_port_port" {
  description = "Port that this service will listen on."
  type        = number
  default     = 9000
}

variable "target_group_load_balancing_algorithm_type" {
  description = "Determines how the load balancer selects targets when routing requests. Only applicable for Application Load Balancer Target Groups. The value is `round_robin` or `least_outstanding_requests`. The default is `round_robin`."
  type        = string
  default     = "round_robin"
}

### TAGS
variable "line_of_business" {
  description = "HIDS LOB that owns the resource."
  type        = string
  default     = "TECH"
}

variable "ado" {
  description = "HIDS ADO that owns the resource. The ServiceNow Contracts table is the system of record for the actual ADO names and LOB names."
  type        = string
  default     = "Kojitechs"
}

variable "tier" {
  description = "Network tier or layer where the resource resides. These tiers are represented in every VPC regardless of single-tenant or multi-tenant. For most resources in the Infrastructure and Security VPC, the TIER will be Management. But in some cases,such as Atlassian, the other tiers are relevant."
  type        = string
  default     = "APP"
}

variable "tech_poc_primary" {
  description = "Email Address of the Primary Technical Contact for the AWS resource."
  type        = string
  default     = "kojitechs@gmail.com"
}

variable "application" {
  description = "Logical name for the application. Mainly used for kojitechs. For an ADO/LOB owned application default to the LOB name."
  type        = string
  default     = "ECS-CLUSTER"
}

variable "builder" {
  description = "The name of the person who created the resource."
  type        = string
  default     = "kojitechs@gmail.com"
}

variable "application_owner" {
  description = "Email Address of the group who owns the application. This should be a distribution list and no an individual email if at all possible. Primarily used for Ventech-owned applications to indicate what group/department is responsible for the application using this resource. For an ADO/LOB owned application default to the LOB name."
  default     = "kojitechs@gmail.com"
}

variable "vpc" {
  description = "The VPC the resource resides in. We need this to differentiate from Lifecycle Environment due to INFRA and SEC. One of \"APP\", \"INFRA\", \"SEC\", \"ROUTING\"."
  type        = string
  default     = "APP"
}

variable "cell_name" {
  description = "The name of the cell."
  type        = string
  default     = "KOJITECHS"
}

variable "master_username" {
  description = "Username for the master DB user"
  type        = string
}

variable "database_name" {
  description = "Name for an automatically created database on cluster creation"
  type        = string
}

variable "django_secret_key" {
  description = "Secret key for Django app"
  default     = "changeme"
}
