aws_account_id = {
  shared = "181437319056"
}

dns_zone_name             = "kojitech.click"
subject_alternative_names = ["*.kojitech.click"]
container_version         = "latest"
master_username           = "recipeapp"
database_name             = "recipeapp"
