
data "aws_iam_policy_document" "ecs_task_execution_role" {

  version = "2012-10-17"
  statement {
    sid     = ""
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com", "ecs.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "iam_for_ecs" {
  name_prefix        = "${var.component_name}-ecsRole"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_execution_role.json
}

resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.iam_for_ecs.name
  policy_arn = aws_iam_policy.ecs.arn
}


resource "aws_iam_policy" "ecs" {
  name_prefix = format("%s-%s", var.component_name, "ecsRole")
  path        = "/"
  description = "IAM policy to allow ${var.component_name}-ecsRole have access"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
          "application-autoscaling:*",
          "autoscaling:*",
          "apigateway:*",
          "cloudfront:*",
          "cloudwatch:*",
          "cloudformation:*",
          "dax:*",
          "dynamodb:*",
          "ec2:*",
          "ec2messages:*",
          "ecr:*",
          "ecs:*",
          "elasticfilesystem:*",
          "elasticache:*",
          "elasticloadbalancing:*",
          "es:*",
          "events:*",
          "iam:*",
          "kms:*",
          "lambda:*",
          "logs:*",
          "rds:*",
          "route53:*",
          "ssm:*",
          "ssmmessages:*",
          "s3:*",
          "sns:*",
          "sqs:*",
          "ec2:DescribeNetworkInterfaces",
          "ec2:CreateNetworkInterface",
          "ec2:DeleteNetworkInterface",
          "ec2:DescribeInstances",
          "ec2:AttachNetworkInterface"
      ],
      "Resource": "*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

# resource "aws_s3_bucket" "app_public_files" {
#   bucket        = "${var.component_name}-files23"
#   acl           = "public-read"
#   force_destroy = true
# }

# data "template_file" "ecs_s3_write_policy" {
#   template = file("./templates/s3-write-policy.json.tpl")

#   vars = {
#     bucket_arn = aws_s3_bucket.app_public_files.arn
#   }
# }

# resource "aws_iam_role" "app_iam_role" {
#   name               = "${var.component_name}-api-task"
#   assume_role_policy = file("./templates/assume-role-policy.json")
# }



# resource "aws_iam_policy" "ecs_s3_access" {
#   name        = "${var.component_name}-AppS3AccessPolicy"
#   path        = "/"
#   description = "Allow access to the recipe app S3 bucket"

#   policy = data.template_file.ecs_s3_write_policy.rendered
# }

# resource "aws_iam_role_policy_attachment" "ecs_s3_access" {
#   role       = aws_iam_role.app_iam_role.name
#   policy_arn = aws_iam_policy.ecs_s3_access.arn
# }
